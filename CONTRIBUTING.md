CONTRIBUTING
===



### 开发环境

OS：windows/linux/macOS

Go：1.8 或以上

本地测试可以使用 ./testdata 作为测试数据，配合 [gobuild](https://github.com/caixw/gobuild) 使用很方便：
`gobuild -ext="go,html,css,yaml" -appdir="./testdata"`。
